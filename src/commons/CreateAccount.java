package commons;

import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class CreateAccount {
    static private String userName;
    static private String password;
    static private String retypePassword;
    static private double initialDepositAmount;
    static private String address;
    static private String phone;
    static private int accountNumber ;

    static HashMap<String, String> allUserDetails = new HashMap<>();

    static Scanner scanner = new Scanner(System.in);

    public CreateAccount(String userName, String password, String retypePassword, double initialDepositAmount, String address, String phone){
        userName = userName;
        password = password;
        retypePassword = retypePassword;
        initialDepositAmount = initialDepositAmount;
        address = address;
        phone = phone;
        accountNumber = generateAccountNumber();
    }
    static int generateAccountNumber(){
        Random randNum = new Random();
        accountNumber = randNum.nextInt(1,14);
       return accountNumber;
    }
    static HashMap inputUserDetails() {
        System.out.println("#### CREATING AN ACCOUNT ####");
        System.out.println("#### Provide Your details to continue ####");
        System.out.println("Your username: ");
         userName = scanner.nextLine();

        boolean passwordDoNoMatch = true;
        while (passwordDoNoMatch){
            System.out.print("Your password: ");
            password = scanner.nextLine();

            System.out.print("Your re-type password: ");
            retypePassword = scanner.nextLine();

            if (!password.equals(retypePassword)){
                System.out.print(password);
                System.out.print(retypePassword);
                System.out.println("Password do not match. Try again: ");
            }
            else{passwordDoNoMatch = false;}
        }




        System.out.print("Your Address: ");
        address = scanner.nextLine();

        System.out.print("Your Phone number: ");
        phone = scanner.nextLine();

        System.out.print("Your Initial Amount to deposit: ");
        initialDepositAmount = scanner.nextDouble();

        allUserDetails.put("userName", userName);
        allUserDetails.put("password", password);

        allUserDetails.put("initialDepositAmount", String.valueOf(initialDepositAmount));
        allUserDetails.put("address", address);
        allUserDetails.put("phone", phone);
        allUserDetails.put("generateAccountNumber", String.valueOf(generateAccountNumber()));
        System.out.println("This is the array "+ allUserDetails);
        return allUserDetails;

    }

    public static HashMap<String, String> getAllUserDetails() {
        return allUserDetails;
    }
}
