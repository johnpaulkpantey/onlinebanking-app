package DBConnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class connectToDb {
    Statement statement;
    Connection connection = null;
    public connectToDb(boolean testOrNotTest){
        if (testOrNotTest == false){
            connect();
            CreateTables createTables = new CreateTables(connection);
        }
    }
    public  Connection connect(){

        try{
           Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/onlinebankingdbtest","onlinebankuser","vpIV%ga}[E^o&Y}");

        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully: ");
        return connection;
    }

    public void closeConnection() {
        if (connection == null) return;
        try {
            connection.close();
            connection = null;
        } catch(SQLException ex) {
            ex.printStackTrace();
        }
    }


}
