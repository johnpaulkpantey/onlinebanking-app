package DBConnect;

import java.sql.Connection;
import java.sql.Statement;

public class CreateTables {

    Statement statement;

    public CreateTables(Connection connection){
         createUserTable(connection);
        createAccountTable(connection);
    }
    public String createUserTable( Connection connection){

        try {
            String query ="CREATE TABLE IF NOT EXISTS  users (userId  serial primary key, username varchar(50) NOT NULL, usertype varchar(10)  NOT NULL, address varchar(100) NOT NULL, phone varchar(12) NOT NULL, created_on timestamp)";
            statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println("user table created");
            return "user table created";
        }catch (Exception e){
            System.out.println("Failed to create user table due to: "+e);
            return "Failed to create user table";
        }

    }
    public String createAccountTable(Connection connection){
        try {
            String query ="CREATE TABLE IF NOT EXISTS account (userId int primary key, foreign key(userId) references users(userId), amount money  NOT NULL, accountNumber varchar(13) NOT NULL)";
            statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println("account table created");
            return "account table created";
        }catch (Exception e){
            System.out.println("Failed to create  account table due to: "+e);
            return "Failed to create account table";
        }

    }
}
